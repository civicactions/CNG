ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITLAB_EXPORTER_VERSION
ARG GITLAB_USER=git

LABEL source="https://gitlab.com/gitlab-org/gitlab-exporter" \
      name="GitLab Exporter" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_EXPORTER_VERSION} \
      release=${GITLAB_EXPORTER_VERSION} \
      summary="Prometheus Web exporter for GitLab." \
      description="Prometheus Web exporter for GitLab."

ENV CONFIG_TEMPLATE_DIRECTORY=/var/opt/gitlab-exporter/templates
ENV CONFIG_DIRECTORY=/etc/gitlab-exporter
ENV CONFIG_FILENAME=gitlab-exporter.yml

ADD gitlab-exporter.tar.gz /

COPY scripts/ /scripts/

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf --disableplugin=subscription-manager --nogpgcheck install -yb --nodocs procps uuid \
    && adduser -m ${GITLAB_USER} \
    && mkdir -p /var/log/gitlab ${CONFIG_DIRECTORY} \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} /var/log/gitlab ${CONFIG_DIRECTORY}

USER ${GITLAB_USER}:${GITLAB_USER}

CMD /usr/bin/gitlab-exporter web -c ${CONFIG_DIRECTORY}/${CONFIG_FILENAME}

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
