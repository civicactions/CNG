ARG RAILS_IMAGE=

FROM ${RAILS_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git

LABEL source="https://gitlab.com/gitlab-org/gitlab" \
      name="GitLab Task Runner" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Task Runner is an entry point for interaction with other containers in the cluster." \
      description="Task Runner is an entry point for interaction with other containers in the cluster. It contains scripts for running Rake tasks, backup, restore, and tools to intract with object storage."

ADD gitlab-task-runner-ee.tar.gz /
ADD gitlab-python.tar.gz /

COPY scripts/bin/* /usr/local/bin/
COPY scripts/lib/* /usr/lib/ruby/vendor_ruby

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf --disableplugin=subscription-manager --nogpgcheck install -yb --nodocs ca-certificates openssl

USER ${GITLAB_USER}:${GITLAB_USER}

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
