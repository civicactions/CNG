ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITLAB_SHELL_VERSION
ARG GITLAB_USER=git
ARG RHEL_REPOSITORY=https://downloads.redhat.com/redhat/rhel/rhel-8-beta/rhel-8-beta.repo

LABEL source="https://gitlab.com/gitlab-org/gitlab-shell" \
      name="GitLab Shell" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_SHELL_VERSION} \
      release=${GITLAB_SHELL_VERSION} \
      summary="SSH access and repository management app for GitLab." \
      description="SSH access and repository management app for GitLab."

ADD gitlab-shell.tar.gz /

COPY scripts/ /scripts/
COPY sshd_config /etc/ssh/

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf --disableplugin=subscription-manager --nogpgcheck install -yb --nodocs procps \
    && dnf --disableplugin=subscription-manager config-manager --add-repo ${RHEL_REPOSITORY} \
    && dnf --disableplugin=subscription-manager --enablerepo=rhel-8-for-x86_64-* --disablerepo=ubi-8-* --nogpgcheck install -yb --nodocs openssh-server \
    && rm /usr/libexec/openssh/ssh-keysign \
    && adduser -m ${GITLAB_USER} \
    && mkdir -p /srv/sshd /var/log/gitlab-shell \
    && touch /var/log/gitlab-shell/gitlab-shell.log \
    && mv /scripts/authorized_keys /authorized_keys \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} /srv/sshd /srv/gitlab-shell /var/log/gitlab-shell /etc/ssh /scripts \
    && chmod 0755 /authorized_keys

ENV CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab-shell

USER ${GITLAB_USER}:${GITLAB_USER}

CMD "/scripts/process-wrapper"

VOLUME /var/log/gitlab-shell

HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD /scripts/healthcheck
