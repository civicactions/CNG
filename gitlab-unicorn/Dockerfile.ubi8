ARG RAILS_IMAGE=

FROM ${RAILS_IMAGE}

ARG GITLAB_CONFIG=/srv/gitlab/config
ARG GITLAB_VERSION
ARG GITLAB_USER=git

LABEL source="https://gitlab.com/gitlab-org/gitlab" \
      name="GitLab Unicorn" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="GitLab Unicorn run the GitLab Rails application with Unicorn web server." \
      description="GitLab Unicorn run the GitLab Rails application with Unicorn web server."

ADD gitlab-unicorn-ee.tar.gz /
ADD gitlab-python.tar.gz /

COPY scripts/ /scripts
COPY unicorn.rb ${GITLAB_CONFIG}/unicorn.rb

ENV GITALY_FEATURE_DEFAULT_ON=1

RUN dnf clean all \
    && rm -r /var/cache/dnf \
    && dnf --disableplugin=subscription-manager --nogpgcheck install -yb --nodocs procps \
    && cd /srv/gitlab \
    && mkdir -p public/uploads \
    && chmod 0700 public/uploads \
    && chown -R ${GITLAB_USER}:${GITLAB_USER} public/uploads

USER ${GITLAB_USER}:${GITLAB_USER}

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck

CMD /scripts/process-wrapper
